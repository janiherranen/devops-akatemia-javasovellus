# Devops Akatemia Java-sovellus

## Lokaali ajo
```
// Suorita samasta kansiosta, missä pom.xml sijaitsee
mvn clean spring-boot:run

// Mene selaimella osoitteeseen localhost:8080/hello?name=Testaaja
// Sivulla pitäisi näkyä "Hello Testaaja!"
// Ilman parametreja pitäisi näkyä "Hello World!
```

## Luo tomcat asennuspaketti (war)
```
mvn clean install

// Komento ajaa yksikkötestit ja luo projektin juureen "target" kansion, jonka sisällä on "academy-java-app.war"
```